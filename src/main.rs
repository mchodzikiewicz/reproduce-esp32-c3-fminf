#![no_std]
#![no_main]

use esp32c3_hal::{
    prelude::*,
};
use esp_backtrace as _;


#[entry]
fn main() -> ! {
    let limit = core::hint::black_box(0f32);
    let value = core::hint::black_box(0f32);
    let f = limit.min(value);
    loop {}
}
